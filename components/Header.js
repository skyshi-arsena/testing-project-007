import Link from 'next'

const linkStyle = {
  marginRight: 15 // some inline css here
}

const Header = () => (
  <div>
    <Link href='/'>
      <a style={linkStyle}>Home</a>
    </Link>
    <Link href='/about'>
      <a style={linkStyle}>About</a>
    </Link>
  </div>
)

export default Header
