import Header from './Header'

const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '2px solid #CDD'
}

const Layout = () => (
  <div style={layoutStyle}>
    <Header />
  </div>
)

export default Layout
