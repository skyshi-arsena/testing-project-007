import Layout from '../components/MyLayout.js'
import Link from 'next/link'

function getPosts () {
  return [
    { id: 'post-link-placeholder', title: 'Hello World' },
    { id: 'hello-zeitgeist', title: 'Routing with NextJS' },
    { id: 'deploy-zeit-apps', title: 'You can deploy apps with Zeit' }
  ]
}

const PostLink = ({post}) => (
  <li>
    <Link as={`/p/${post.id}`} href={`/post?title=${post.title}`}>
      <a>{post.title}</a>
    </Link>
  </li>
)

export default () => (
  <Layout>
    <h1>Muh blog</h1>
    <ul>
      {getPosts().map((post) => (
        <PostLink key={post.id} post={post} />
      ))}
    </ul>
    <style jsx global>{`
      h1 {background: red;}
      ul {background: blue;}
      li {background: yellow;}
    `}
    </style>
  </Layout>
)
